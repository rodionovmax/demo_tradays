package com.demo.ui.test;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

public class DeviceCapabilities {

    public DevicePlatform devicePlatform;
    public AndroidEmulator androidEmulator;
    public AndroidRealDevice androidRealDevice;
    public AndroidVersion androidVersion;

    private AndroidDriver driver;

    public static final String APPIUM_URL = "http://127.0.0.1:4723/wd/hub";
    public static final String CURRENT_BUILD_ANDROID = "tradays.apk";

    public enum DevicePlatform {
        EMULATOR, REAL_DEVICE
    }

    public enum AndroidRealDevice {
        GALAXY_S9
    }

    public enum AndroidEmulator {
        NEXUS_5X, PIXEL_3A
    }

        public enum AndroidVersion {
        ANDROID_8_0, ANDROID_9_0, ANDROID_10_0
    }

    public AndroidDriver getDriver(String platform, String device, String os_version) throws Exception {
        URL URL = new URL(APPIUM_URL);

        if (platform.equalsIgnoreCase("real_device")) {
            devicePlatform = DevicePlatform.REAL_DEVICE;
        } else if (platform.equalsIgnoreCase("emulator")) {
            devicePlatform = DevicePlatform.EMULATOR;
        } else {
            System.out.println("Platform parameter wasn't recognized. Launching default emulator");
            devicePlatform = DevicePlatform.EMULATOR;
        }

        if (os_version.equalsIgnoreCase("android 8.0")) {
            androidVersion = AndroidVersion.ANDROID_8_0;
        } else if (os_version.equalsIgnoreCase("android 9.0")) {
            androidVersion = AndroidVersion.ANDROID_9_0;
        } else if (os_version.equalsIgnoreCase("android 10.0")) {
            androidVersion = AndroidVersion.ANDROID_10_0;
        } else {
            System.out.println("Android Version parameter wasn't recognized. Launching default Android 10.0");
            androidVersion = AndroidVersion.ANDROID_10_0;
        }

        if (device.equalsIgnoreCase("nexus 5X")) {
            androidEmulator = AndroidEmulator.NEXUS_5X;
        } else if (device.equalsIgnoreCase("pixel 3a")) {
            androidEmulator = AndroidEmulator.PIXEL_3A;
        } else if (device.equalsIgnoreCase("galaxy s9")) {
            androidRealDevice = AndroidRealDevice.GALAXY_S9;
        } else {
            System.out.println("Device parameter wasn't recognized. Launching default device Pixel 3A");
            androidEmulator = AndroidEmulator.PIXEL_3A;
        }

        switch (devicePlatform) {

            case EMULATOR:
                switch (androidVersion) {

                    case ANDROID_9_0:
                        switch (androidEmulator) {
                            case NEXUS_5X:
                                driver = new AndroidDriver(URL, this.getCapabilitiesForNexus5XAndroid9());
                                break;
                            case PIXEL_3A:
                                driver = new AndroidDriver(URL, this.getCapabilitiesForPixel3AAndroid9());
                                break;
                        }
                        break;

                    case ANDROID_10_0:
                        switch (androidEmulator) {
                            case NEXUS_5X:
                                driver = new AndroidDriver(URL, this.getCapabilitiesForNexus5XAndroid10());
                                break;
                            case PIXEL_3A:
                                driver = new AndroidDriver(URL, this.getCapabilitiesForPixel3AAndroid10());
                                break;
                        }
                        break;
                }
                break;

            case REAL_DEVICE:
                switch (androidVersion) {

                    case ANDROID_8_0:
                        switch (androidRealDevice) {
                            case GALAXY_S9:
                                driver = new AndroidDriver(URL, this.getCapabilitiesForGalaxyS9Android8());
                                break;
                        }
                        break;

                }
                break;
        }

        return driver;
    }

    private DesiredCapabilities getCapabilitiesForNexus5XAndroid9() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Nexus 5X");
        capabilities.setCapability("platformVersion", "9.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "net.metaquotes.economiccalendar");
        capabilities.setCapability("appActivity", "net.metaquotes.ui.MainActivity");
        capabilities.setCapability("app", System.getProperty("user.dir") + "/apks/" + CURRENT_BUILD_ANDROID);
        return capabilities;
    }

    private DesiredCapabilities getCapabilitiesForNexus5XAndroid10() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Nexus 5X");
        capabilities.setCapability("platformVersion", "10.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "net.metaquotes.economiccalendar");
        capabilities.setCapability("appActivity", "net.metaquotes.ui.MainActivity");
        capabilities.setCapability("app", System.getProperty("user.dir") + "/apks/" + CURRENT_BUILD_ANDROID);
        return capabilities;
    }

    private DesiredCapabilities getCapabilitiesForPixel3AAndroid9() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Pixel 3A");
        capabilities.setCapability("platformVersion", "10.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "net.metaquotes.economiccalendar");
        capabilities.setCapability("appActivity", "net.metaquotes.ui.MainActivity");
        capabilities.setCapability("app", System.getProperty("user.dir") + "/apks/" + CURRENT_BUILD_ANDROID);
        return capabilities;
    }

    private DesiredCapabilities getCapabilitiesForPixel3AAndroid10() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Pixel 3A");
        capabilities.setCapability("platformVersion", "10.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "net.metaquotes.economiccalendar");
        capabilities.setCapability("appActivity", "net.metaquotes.ui.MainActivity");
        capabilities.setCapability("app", System.getProperty("user.dir") + "/apks/" + CURRENT_BUILD_ANDROID);
        return capabilities;
    }

    private DesiredCapabilities getCapabilitiesForGalaxyS9Android8() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "My Samsung Galaxy S9");
        capabilities.setCapability("udid", "3830563551313498");
        capabilities.setCapability("platformVersion", "8.0.0");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", "net.metaquotes.economiccalendar");
        capabilities.setCapability("appActivity", "net.metaquotes.ui.MainActivity");
        return capabilities;
    }

}
