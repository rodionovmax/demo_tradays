package com.demo.ui.test.tests;

import com.demo.ui.test.CoreTestBase;
import com.demo.ui.test.Data;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EndToEndTests extends CoreTestBase {

    @Test
    public void testFilters() {

        // Step 2. Verify English language is chosen in Settings
        nav.goToSettings();

        Assert.assertTrue(settings.getStringsForTimeZoneAndLanguage().contains(Data.expectedLanguage),
                "The actual Language doesn't match with " + Data.expectedLanguage);

        // Step 3. Filter by parameters Importance: Medium and Country: Switzerland
        nav.goToCalendar();
        calendar.clickFilters();
        filter.selectMediumImportance();
        filter.selectSwitzerland();
        nav.goBack();

        // Step 4. Open first event for Switzerland
        calendar.clickOnFirstEvent();

        // Step 5. Verify that importance and country are equal Medium and Switzerland
        Assert.assertTrue(calendar.getImportanceInEvent().equalsIgnoreCase(Data.filterImportanceMedium),
                "Event importance < " + calendar.getImportanceInEvent() + " > doesn't match with " + Data.filterImportanceMedium);

        Assert.assertTrue(calendar.getCountryInEvent().equalsIgnoreCase(Data.filterCountrySwitzerland),
                "Event country < " + calendar.getCountryInEvent() + " > doesn't match with " + Data.filterCountrySwitzerland);

        // Step 6. Log the history of last 12 months
        calendar.clickHistoryTab();
    }
}
