package com.demo.ui.test;

import com.demo.ui.test.pageobjects.*;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.lang.reflect.Method;

public class CoreTestBase {

    protected AndroidDriver driver;

    protected MainPageObject main;
    protected AlertsPO alerts;
    protected CalendarPO calendar;
    protected SettingsPO settings;
    protected FiltersPO filter;
    protected Navigation nav;
    protected TradePO trade;

    public DeviceCapabilities capabilities = new DeviceCapabilities();

    @BeforeMethod
    @Parameters({"platform", "device", "os_version"})
    public void setupAppium(@Optional("emulator") String platform,
                            @Optional("pixel 3a") String device,
                            @Optional("android 10.0") String os_version,
                            Method method, ITestContext context) throws Exception {

        Reports.start(method.getName());

        // Initialize driver with capabilities
        driver = capabilities.getDriver(platform, device, os_version);

        main = new MainPageObject(driver);
        settings = new SettingsPO(driver);
        calendar = new CalendarPO(driver);
        filter = new FiltersPO(driver);
        nav = new Navigation(driver);

    }

    @AfterMethod
    public void tearDown(ITestResult testResult, Method method) {

        if(testResult.getStatus()==ITestResult.FAILURE){
            Reports.fail(driver, testResult.getName());
        }
        Reports.stop();
    }
}
