package com.demo.ui.test.pageobjects;

import com.demo.ui.test.Locators;
import com.demo.ui.test.MainPageObject;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

import java.util.List;


public class CalendarPO extends MainPageObject {

    public CalendarPO(AndroidDriver driver) {
        super(driver);
    }

    public void clickFilters() {
        waitForElementAndClick(Locators.FILTERS, 5);
    }

    public void clickOnFirstEvent() {
        List<WebElement> eventTitles = waitForManyElementsPresent(Locators.EVENT_TITLES, 5);
        eventTitles.get(0).click();
    }

    public String getCountryInEvent() {
        return waitForElementPresent(Locators.EVENT_COUNTRY, 5).getText();
    }

    public String getImportanceInEvent() {
        return waitForElementPresent(Locators.EVENT_IMPORTANCE, 5).getText();
    }

    public void clickHistoryTab() {
        waitForElementAndClick(Locators.EVENT_HISTORY_TAB, 5);
    }








}
