package com.demo.ui.test.pageobjects;

import com.demo.ui.test.Locators;
import com.demo.ui.test.MainPageObject;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class SettingsPO extends MainPageObject {

    public SettingsPO(AndroidDriver driver) {
        super(driver);
    }


    public ArrayList<String> getStringsForTimeZoneAndLanguage() {
        List<WebElement> elementsForTimeAndLanguage = waitForManyElementsPresent(Locators.TIME_ZONE_AND_LANGUAGE, 5);
        ArrayList<String> strings = new ArrayList<>();

        for (WebElement element : elementsForTimeAndLanguage) {
            strings.add(element.getText());
        }
        return strings;
    }


}
