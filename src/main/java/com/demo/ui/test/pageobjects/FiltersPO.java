package com.demo.ui.test.pageobjects;

import com.demo.ui.test.Locators;
import com.demo.ui.test.MainPageObject;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class FiltersPO extends MainPageObject {

    public FiltersPO(AndroidDriver driver) {
        super(driver);
    }

    public void selectMediumImportance() {
        // Deselect All Importance
        clickSelectAllInImportanceFilter();
        // Click on Medium checkbox
        findMediumCheckbox().click();
    }

    public void clickSelectAllInImportanceFilter() {
        List<WebElement> selectAll = waitForManyElementsPresent(Locators.FILTERS_SELECT_ALL, 5);
        selectAll.get(0).click();
    }

    public WebElement findMediumCheckbox() {
        List<WebElement> mediumElements = waitForManyElementsPresent(Locators.FILTER_MEDIUM_XPATH, 5);
        return mediumElements.get(2);
    }

    public void selectSwitzerland() {
        // Deselect All Countries
        clickSelectAllInCountryFilter();
        // Swipe until Switzerland shows up
        swipeUpToFindElement(Locators.FILTER_SWITZERLAND_XPATH, 5);
        // Click on Switzerland checkbox
        waitForManyElementsPresent(Locators.FILTER_SWITZERLAND_NODES, 5).get(2).click();
    }

    public void clickSelectAllInCountryFilter() {
        List<WebElement> selectAll = waitForManyElementsPresent(Locators.FILTERS_SELECT_ALL, 5);
        selectAll.get(1).click();
    }
}
