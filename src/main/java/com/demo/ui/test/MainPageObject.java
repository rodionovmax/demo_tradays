package com.demo.ui.test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class MainPageObject {

    protected RemoteWebDriver driver;

    public MainPageObject(RemoteWebDriver driver) {
        this.driver = driver;
    }

    /**
     * Wait for element to be present with error message
     */
    public WebElement waitForElementPresent(By by, String error_message, long timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.withMessage(error_message + "\n");
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }

    /**
     * Wait for element to be present w/o error message
     */
    public WebElement waitForElementPresent(By by, long timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }

    /**
     * Wait for element to be present w/o error message
     */
    public List<WebElement> waitForManyElementsPresent(By by, long timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by)
        );
    }

    /**
     * Wait for element to be present w/o error message
     */
    public WebElement waitForElementVisible(By by, long timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        return wait.until(
                ExpectedConditions.visibilityOfElementLocated(by)
        );
    }

    /**
     * Wait for element to be present w/o error message with default wait = 5 seconds
     */
    public WebElement waitForElementPresent(By by) {
        return waitForElementPresent(by, 5);
    }

    /**
     * Wait for element to be present and Click
     */
    public void waitForElementAndClick(By by, long timeoutInSeconds) {
        WebElement element = waitForElementPresent(by, timeoutInSeconds);
        element.click();
    }

    /**
     * Wait for element to be present and Send keys
     */
    public void waitForElementAndSendKeys(By by, String value, long timeoutInSeconds) {
        WebElement element = waitForElementPresent(by, timeoutInSeconds);
        element.sendKeys(value);
    }

    /**
     * Wait for element to be present and Clear input
     */
    public WebElement waitForElementAndClear(By by, long timeoutInSeconds) {
        WebElement element = waitForElementPresent(by, timeoutInSeconds);
        element.clear();
        return element;
    }

    /**
     * Method for implicit wait in Seconds
     */
    public void javaWaitInSec(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * == METHODS FOR SWIPES ==
     */


    /**
     * Adjustable Swipe
     */
    public void adjustableSwipe(double y1, double y2, int timeOfSwipe) {
        if (driver instanceof AppiumDriver) {
            TouchAction action = new TouchAction((AppiumDriver) driver);
            Dimension size = driver.manage().window().getSize();
            int x = size.width / 2;
            int start_y = (int) (size.height * y1);
            int end_y = (int) (size.height * y2);

            action.
                    press(PointOption.point(x, start_y))
                    .waitAction(waitOptions(Duration.ofMillis(timeOfSwipe)))
                    .moveTo(PointOption.point(x, end_y))
                    .release()
                    .perform();
        } else {
            System.out.println("Method adjustableSwipe() does nothing for Web platform");
        }

    }

    /**
     * Swipe till element appears in DOM
     */
    public void swipeUpToFindElement(By by, int max_swipes) {
        int already_swiped = 0;
        while (driver.findElements(by).size() == 0) {

            if (already_swiped > max_swipes) {
                System.out.println("Reached max number of swipes");
                waitForElementPresent(by, 0);
                return;
            }
            adjustableSwipe(0.8, 0.3, 600);
            ++already_swiped;
        }
    }

    /**
     * Method to get system date
     */
    public String getCurrentDate() {
        Format formatter = new SimpleDateFormat("dd MMM yyyy");
        String currentDate = formatter.format(new Date());
        System.out.println(currentDate);

        return currentDate;
    }

}
