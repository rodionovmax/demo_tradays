package com.demo.ui.test;

import io.appium.java_client.android.AndroidDriver;

public class Navigation extends MainPageObject {

    public Navigation(AndroidDriver driver) {
        super(driver);
    }

    public void goToCalendar() {
        waitForElementAndClick(Locators.BOTTOM_BAR_CALENDAR, 5);
    }

    public void goToSettings() {
        waitForElementAndClick(Locators.BOTTOM_BAR_SETTINGS, 5);
    }

    public void goBack() {
        waitForElementAndClick(Locators.GO_BACK, 5);
    }
}
