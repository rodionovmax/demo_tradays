package com.demo.ui.test;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class Locators {

    /* Navigation */
    public static final By BOTTOM_BAR_SETTINGS = By.id("net.metaquotes.economiccalendar:id/bottom_bar_settings");
    public static final By BOTTOM_BAR_CALENDAR = By.id("net.metaquotes.economiccalendar:id/bottom_bar_calendar");
    public static final By BOTTOM_BAR_ALERTS = By.id("net.metaquotes.economiccalendar:id/bottom_bar_alerts");
    public static final By GO_BACK = new MobileBy.ByAccessibilityId("Navigate up");

    /* Calendar */
    public static final By FILTERS = new MobileBy.ByAccessibilityId("Filters");
    //    public static final By FILTERS = By.id("net.metaquotes.economiccalendar:id/menu_filter");
    public static final By EVENT_TITLES = By.id("net.metaquotes.economiccalendar:id/title");

    /* Filter */
    public static final By FILTERS_SELECT_ALL = By.id("net.metaquotes.economiccalendar:id/select_all");
    public static final By FILTERS_TITLES = By.id("net.metaquotes.economiccalendar:id/title");
    public static final By TIME_ZONE_AND_LANGUAGE = By.id("net.metaquotes.economiccalendar:id/hint");
    public static final By FILTER_MEDIUM_XPATH = By.xpath("//android.widget.LinearLayout/android.widget.TextView[@text='Medium']/../../child::node()");
    public static final By FILTER_SWITZERLAND_XPATH = By.xpath("//android.widget.LinearLayout/android.widget.TextView[@text='Switzerland']");
    public static final By FILTER_SWITZERLAND_NODES = By.xpath("//android.widget.LinearLayout/android.widget.TextView[@text='Switzerland']/../../child::node()");

    /* Event */
    public static final By EVENT_COUNTRY = By.id("net.metaquotes.economiccalendar:id/country_name");
    public static final By EVENT_IMPORTANCE = By.id("net.metaquotes.economiccalendar:id/event_importance");
    public static final By EVENT_HISTORY_TAB = By.id("net.metaquotes.economiccalendar:id/tab_history");


}
