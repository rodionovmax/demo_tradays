package com.demo.ui.test;

import org.testng.annotations.DataProvider;

public class DataProviders {

    @DataProvider(name = "Filters")
    public static Object[][] FilterParameters(){
        return new Object[][]{
                {Data.filterImportanceMedium, Data.filterCountrySwitzerland},
        };
    }
}
